import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.util.List;
import java.util.Set;

public class BasePage {

    static AndroidDriver _driver;

    public BasePage(AndroidDriver driver){
        _driver = driver;
    }

    public void setContextWeb() { //Estos métodos consumen tiempo y recursos, sería bueno conseguir alternativas, pero sin hacer este barrido, no se reconocen los elementos(Al menos en el caso native es seguro esto, hay que confirmar el web.
        _driver.context("WEBVIEW_coop.bancocredicoop.bancamobile");
        List<MobileElement> elementos = _driver.findElementsByXPath("//*");
    }
    public void setContextNative() {
        _driver.context("NATIVE_APP");
        List<MobileElement> elementos = _driver.findElementsByXPath("//*");
    }

    //Método para intentar reconocer un elemento en su contexto nativo o web por xpath en una sola ejecución de Kobiton para ahorrar minutos.

    public void nativeOrWeb(String nombreElemento, String xpathNative, String xpathWeb) {
        try {
            setContextNative();
            System.out.println(_driver.getContext());
            MobileElement elemento = (MobileElement) _driver.findElementByXPath(xpathNative);
            System.out.println(nombreElemento + " reconocido por xpath en contexto nativo");
        } catch (Exception e) {
            System.out.println(nombreElemento + " NO reconocido por xpath en contexto nativo");
        }
        try {
            setContextWeb();
            System.out.println(_driver.getContext());
            MobileElement elemento = (MobileElement) _driver.findElementByXPath(xpathWeb);
            System.out.println(nombreElemento + " reconocido por xpath en contexto Webview");
        } catch (Exception e) {
            System.out.println(nombreElemento + "NO reconocido por xpath en contexto Webview");
        }
    }
    //De momento no se usa pero puede servir en algún momento.
    public void listElementsByXpath() { //metodo que devuelve todos los elementos que se consigan por xpath, sirve para comparar contexto nativo y WebView.
        List<MobileElement> elementos = _driver.findElementsByXPath("//*");
        System.out.print("numero elementos: ");
        System.out.println(elementos.size());
        System.out.print("Contexto: ");
        System.out.println(_driver.getContext());
        for (MobileElement elemento : elementos) {
            System.out.println(elemento.getText());
        }
    }
    //No se aplica ni funciona realmente pq native siempre es index 0 y web siemrpe es index 1. Pero bueno, por si sirve de algo
    public void obtenerContextos() throws InterruptedException {
        System.out.print("contexto inicial: ");
        System.out.println(_driver.getContext());
        Set<String> contextNames = _driver.getContextHandles();
        System.out.print("Contextos: ");
        for (String contextName : contextNames) {
            System.out.println(contextName + " - ");
        }
        _driver.context((String) contextNames.toArray()[1]);
        System.out.print("Nuevo contexto: ");
        System.out.println(_driver.getContext());
    }

}
