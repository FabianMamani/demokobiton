import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

public class Home extends BasePage{

    public Home(AndroidDriver driver) {
        super(driver);
    }

    By menuLateral = By.xpath("//*[@text='Menú lateral']"); //native
    //MobileElement salir = (MobileElement) _driver.findElementByAccessibilityId("Salir"); //No se puede guardar el By a parte con este tipo de búsqueda por la estructura del mismo..

    public void cuentas(){
        String nombreAdherente;
        String tipoCuenta;
        String saldo;
        String tipoCuentaDolares;
        String saldoDolares;

        setContextNative();
        nombreAdherente = _driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]").getText();
        System.out.println(nombreAdherente);
        setContextWeb();
        tipoCuenta = _driver.findElementByXPath("//ion-nav-view[@name=\"menuContent\"]/ion-view/ion-content/div/div[2]/div/div[1]/div[1]/span").getText();
        System.out.println(tipoCuenta);
        saldo = _driver.findElementByXPath("//ion-nav-view[@name=\"menuContent\"]/ion-view/ion-content/div/div[2]/div/div[1]/div[2]/span").getText();
        System.out.println(saldo);
        tipoCuentaDolares = _driver.findElementByXPath("//ion-nav-view[@name=\"menuContent\"]/ion-view/ion-content/div/div[2]/div/div[2]/div[1]/span").getText();
        System.out.println(tipoCuentaDolares);
        saldoDolares = _driver.findElementByXPath("//ion-nav-view[@name=\"menuContent\"]/ion-view/ion-content/div/div[2]/div/div[2]/div[2]/span").getText();
        System.out.println(saldoDolares);
    }



}
