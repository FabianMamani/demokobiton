import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

public class Transferencias extends BasePage{

    public Transferencias(AndroidDriver driver) {
        super(driver);
    }

//---------------------------------------------------------------------------------------------------------------------
//    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<menu de transferencias>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//----------------------------------------------------------------------------------------------------------------------
    //en pantalla "banco credicoop"
    public void selectCuentasDelMismoBanco() throws InterruptedException {
        By _entreMismoBanco = By.xpath("//ion-nav-view[@name=\"menuContent\"]/ion-nav-view/ion-view/ion-content/div/div/a[1]/div");
    ////android.view.View[@content-desc="Transferencias Dentro de Banco Credicoop"]/android.view.View xpath alternativo :P
        setContextWeb();
        _driver.findElement(_entreMismoBanco).click();
        Thread.sleep(5000);
        System.out.println("llegamos a cuentas del mismo banco!!!!");

    }

    //en pantalla "a cuentas de otros bancos"
    public void selectCuentasOtrosBancos() throws InterruptedException {


    }

    //en pantalla "Pago de haberes"
    public void selectPagoHaberes() throws InterruptedException {


    }
    //en pantalla "punto efectivo"
    public void selectPuntoEfectivo() throws InterruptedException {


    }

//--------------------------------------------------------------------------------------------------------------------
    // >>>>>>>>>>>>>>sector de transferencias a cuentas del mismo banco<<<<<<<<<<<<<<<<<<<<<
//--------------------------------------------------------------------------------------------------------------------

    //en pantalla "a cuentas Propias"
    public void selectCuentasPropias() throws InterruptedException {


    }


    //en pantalla "a cuentas de terceros"
    //se accede a la carga de datos de la transferencia
    public void selectCuentasTerceros() throws InterruptedException {
       //TODO // By _cuentasTerceros = MobileBy.ByAccessibilityId("A Cuentas de Terceros");
        By _cuentasTerceros = By.xpath("//android.view.View[@content-desc=\"A Cuentas de Terceros\"]");

        setContextNative();
        _driver.findElement(_cuentasTerceros).click();
        Thread.sleep(5000);
        System.out.println("llegamos a cuentas de terceros!!!!!");

    }



// { }---------------------------------------------------------------------------------------------------------------------
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Carga de datos de transferencia>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//---------------------------------------------------------------------------------------------------------------------
    public void realizarTransferencia() throws InterruptedException {

        By _campoCuentaOrigen = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.Button[1]");
        By _cuentaOrigenPesosRB = By.xpath("(//input[@name=\"group\"])[1]");
        By _cuentaOrigenPesosSpan = By.xpath("/html/body/div[4]/div[2]/ion-modal-view/ion-content/div/ion-list/div/ion-item[1]/span[1]");
        By _campoCuentaDestino =By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.Button[2]");
        By _cuentaDestinoRB= By.xpath("(//input[@name=\"group\"])[3]");
        By _cuentaDestinoSpan= By.xpath("/html/body/div[5]/div[2]/ion-modal-view/ion-content/div/ion-list/div/ion-item[1]/span[2]");
        By _cuentaDestinoDivBeneficiario = By.xpath("/html/body/div[5]/div[2]/ion-modal-view/ion-content/div/ion-list/div/ion-item[1]/div");
        By _campoMontoEnteros = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[4]/android.widget.EditText");
        By _campoMontoDecimales = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[6]/android.widget.EditText");
        By _calendario = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[7]/android.view.View[2]/android.widget.Button");
        By _diaCalendario = By.xpath("//*[@text='25']");
        By _campoObservaciones = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[9]/android.widget.EditText");
        By _campoEmail = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[11]/android.view.View/android.widget.EditText");
        By _botonContinuar = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.Button[3]");
        By _botonVerificarConfirmar = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.Button");
        By _campoContraseñaConfirmarOperacion = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.widget.EditText");
        By _botonTransferir = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.Button");
        By _txtNumOperacionNative = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View");
        By _txtNumOperacionWeb = By.xpath("//ion-nav-view[@name=\"menuContent\"]/ion-nav-view/ion-view/ion-content/div/div/div/div/div[1]/div[1]/div ");
        By _botonIrInicioDespuesTransferir = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[4]/android.widget.Button[2]");


        //TODO: Extraer numero de cuenta en seleccionar cuenta origen, comparar con la que se tiene en home y seleccionar la apropiada.
         setContextNative();
        _driver.findElement(_campoCuentaOrigen).click();
        _driver.context("WEBVIEW_coop.bancocredicoop.bancamobile");
        String text = _driver.findElement(_cuentaOrigenPesosSpan).getText();
        System.out.println("texto del span cuenta origen: " + text);
        _driver.findElement(_cuentaOrigenPesosRB).click();


        _driver.context("NATIVE_APP");
        _driver.findElement(_campoCuentaDestino).click();
        setContextWeb();
        String textoBeneficiario = _driver.findElement(_cuentaDestinoDivBeneficiario).getText();
        System.out.println("texto del div cuenta destino: " + textoBeneficiario);
        String textocta = _driver.findElement(_cuentaDestinoSpan).getText();
        System.out.println("texto del span cuenta destino: " + textocta);
        _driver.findElement(_cuentaDestinoRB).click();

        Thread.sleep(4000);

        _driver.context("NATIVE_APP");
        _driver.findElement(_campoMontoEnteros).sendKeys("1000");
        _driver.findElement(_campoMontoDecimales).sendKeys("00");
        _driver.findElement(_calendario).click();
        _driver.findElement(_diaCalendario).click();
        _driver.findElement(_campoObservaciones).sendKeys("observaciones");
        _driver.findElement(_campoEmail).sendKeys("testingusuario@gmail.com");
        _driver.findElement(_botonContinuar).click();

        _driver.findElement(_botonVerificarConfirmar).click();

        Thread.sleep(5000);

       _driver.findElement(_campoContraseñaConfirmarOperacion).sendKeys("testing6");
      //TODO: este botòn tiene el mismo xpath del que dices "otra transferecnia cuando hay error del servidor por fecha del dispositivo"
       _driver.findElement(_botonTransferir).click();
        Thread.sleep(15000);

        setContextNative();
        try {

            String texto = _driver.findElement(_txtNumOperacionNative).getText();
            System.out.println("texto native: " + texto);

        } catch (Exception e) {
            System.out.println("no funcionò el reconocimiento native");
        }

        setContextWeb();
        try {

            String texto = _driver.findElement(_txtNumOperacionWeb).getText();
            System.out.println("texto web: " + texto);

        } catch (Exception e) {
            System.out.println("no funcionò el reconocimiento web");
        }
        _driver.context("NATIVE_APP");


        try {

           _driver.findElement(_botonIrInicioDespuesTransferir).click();

        } catch (Exception e) {
            System.out.println("no funcionò el boton ir a inicio.");
        }
        Thread.sleep(10000);


    }
//---------------------------------------------------------------------------------------------------------------------

}
