import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

public class LateralMenu extends BasePage{
    public LateralMenu(AndroidDriver driver) {
        super(driver);
    }


    public void openMenu()throws InterruptedException{
        By menuLateral = By.xpath("//*[@text='Menú lateral']"); //native
        setContextNative();
        _driver.findElement(menuLateral).click();
    }

    public void menuTransferencias() throws InterruptedException {
        By transferencias = By.xpath("/html/body/ion-nav-view/ion-side-menus/ion-side-menu/ion-content/div/div[1]/div[3]");

        openMenu();
        setContextWeb();
        _driver.findElement(transferencias).click();
        Thread.sleep(5000);
        System.out.println("llegamos a menu transferencias");
    }

    public void menuCuentas() throws InterruptedException {
        By cuentas = By.xpath("/html/body/ion-nav-view/ion-side-menus/ion-side-menu/ion-content/div/div[1]/div[1]");

        openMenu();
        setContextWeb();
        _driver.findElement(cuentas).click();
        Thread.sleep(5000);

    }


    //Incluir logOut en BasePage? De modo que peuda ser llamado dese cualquier ubicación de la app. Hay que confirmar que el identificador siga siendo el mismo independientmente de la pantalla.
    public void logOut() throws InterruptedException {
        By logOutBtn =By.xpath("/html/body/ion-nav-view/ion-side-menus/ion-side-menu/ion-footer-bar/a");
        By confirmLogOut =By.xpath("/html/body/div[4]/div/div[3]/button[2]");

        openMenu();
        setContextWeb();
        _driver.findElement(logOutBtn).click(); //Botón salir
        //agregar waits explicitos
        Thread.sleep(5000);
        _driver.findElement(confirmLogOut).click();   // botón si de confirmación
    }

}


