import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

import java.util.List;

public class Login extends BasePage{

    //Según POM acá deberíamos guardar los By de los distintos objetos de cada página.. pero como los estamos scando de listas por clases no estoy seguro de cómo implementarlo.

    public Login(AndroidDriver driver){
        super(driver);

    }

    public void webView(){ // según este link http://appium.io/docs/en/writing-running-appium/web/hybrid/ (al que nos dirigió appium) tenemos que setear la propiedad setWebContentsDebuggingEnabled del WebView como true para poder debuggear mejor.. hay que investigar esto.
        MobileElement webViewElement = (MobileElement) _driver.findElementByClassName("android.webkit.WebView");
        }

    public void pressEntendido() throws InterruptedException {
        Thread.sleep(5000);
        //setContextWeb();
       // _driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/button")).click();
        setContextNative();
        //_driver.context("WEBVIEW_coop.bancocredicoop.bancamobile");
        //_driver.context("NATIVE_APP");
        _driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[3]/android.widget.Button")).click();

    }

    public void setUserAndPass () {
        setContextNative();
        List<MobileElement> _campos = _driver.findElementsByClassName("android.widget.EditText");
        System.out.print("Número de elementos con la clase android.widget.EditText: ");
        System.out.println(_campos.size());

        _campos.get(0).sendKeys("20144835");
        _campos.get(1).sendKeys("testing6");
            for (MobileElement campo : _campos){
            System.out.println(campo.getText());
        }
        //  _driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.widget.EditText").sendKeys("Fabian");
    }

    public void pressLoginButton() throws InterruptedException {
        setContextNative();
        List<MobileElement> _buttons = _driver.findElementsByClassName("android.widget.Button");
        System.out.print("Número de elementos con la clase android.widget.Button: ");
        System.out.println(_buttons.size());
        _buttons.get(3).click();
        Thread.sleep(10000);

    }
    public void loginAlertExceptions() {
        try { //Ventana emergente de actualiación de app
            setContextWeb();
            MobileElement alertaVersionDesactualizada = (MobileElement) _driver.findElementByXPath("/html/body/div[4]/div/div[3]/button[2]");
            alertaVersionDesactualizada.click();
            System.out.println("La versión de la App no es la más actualizada");
        } catch (Exception e) {
          }
        try { //Ventana emergente de "Momentaneamente no se encuentra disponible la información solicitada"  <-- funcionamiento confirmado
            setContextWeb();
            MobileElement alertaMomentaneamente = (MobileElement) _driver.findElementByXPath("/html/body/div[3]/div/div[3]/button");
            alertaMomentaneamente.click();
            System.out.println("App momentaneamente no disponible");
        } catch (Exception e) {
          }
    //TODO:Cuando no estè disponible la app que jecute otro tlf
        //TODO: agarrar texto de momentaneamente vs el de dispositivo fuera de lìnea para diferenciar excepciones.
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //Incluir excepción de dispositivo fuera de línea
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
}
