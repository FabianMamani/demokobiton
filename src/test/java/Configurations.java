import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import java.io.IOException;


public class Configurations {

    //Inicializar driver

    /*Este if es para cuando se incluyan pruebas en iphone. Va a ser necesario que la variable platformName
     sea definida en el archivo configs o algún lugar donde pueda ser usado acá y en SetUpDevice

     Se agregaría un if en el setUpDriver tambien


    if (platformName = "Android"){
    public static AndroidDriver<AndroidElement> driver;
     } else {
     public static IOSDriver<IOSElement> driver;
       }*/

    public static AndroidDriver<AndroidElement> driver;

    @BeforeTest
    public static void setUpDriver() throws IOException, InterruptedException {
        SetUpDevice.getEspecificDevice();

        driver = new AndroidDriver<>(SetUpDevice.kobitonServerUrl(), SetUpDevice.setCapabilities());
    }

    @AfterTest
    public static void closeDriver(){
        driver.quit();
        System.out.println("Dispositivo desconectado");
    }

}
