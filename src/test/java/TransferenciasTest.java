import org.testng.annotations.Test;


public class TransferenciasTest extends Configurations{

    @Test
    public void transferenciaTest() throws InterruptedException {

         LateralMenu LateralMenu = new LateralMenu(driver);
         Transferencias Transferencias = new Transferencias(driver);


         LateralMenu.menuTransferencias();
         Transferencias.selectCuentasDelMismoBanco();
         Transferencias.selectCuentasTerceros();
         Transferencias.realizarTransferencia();

    }

}
